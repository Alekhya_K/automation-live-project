import java.util.Random;
import java.util.Scanner;

public class Test {
    public static void main(String args[]) {
        int rand = new Random().nextInt(5);
        Scanner sc= new Scanner(System.in);
        String input = sc.nextLine();
        System.out.println(rand);
        if(input.equalsIgnoreCase("Incoming Call")) {
            if(rand == 1) {
                System.out.println("Representative R1 assigned to call");
            }
            else if(rand == 2) {
                System.out.println("All representatives and managers are busy. Assigning call to Manager M1");
            }
            else if(rand == 3) {
                System.out.println("All representatives and managers are busy. Assigning call to senior Manager SM");
            }
            else if(rand == 4) {
                System.out.println("All representatives, managers, senior managers are busy. Assigning call to wait queue");
            }
        }
        else if(input.equalsIgnoreCase("End call to R1")) {
            System.out.println("Representative R1's call is terminated");
        }
        else if(input.equalsIgnoreCase("End call SM")) {
            System.out.println("Senior Manager SM1 call is terminated. A new call from wait queue is assigned to SM!");
        }
    }
}
