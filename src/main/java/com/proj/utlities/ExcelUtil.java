package com.proj.utlities;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

public class ExcelUtil {
    public static String screenshotName;

    public static Object[][] getExcel(String path, String sheetName) throws IOException {
        //identify the firstname column

        path = "C:\\Users\\1646238\\IdeaProjects\\AutomationLiveProjects\\src\\main\\resources\\excel\\testdata.xls";
        FileInputStream inputStream = new FileInputStream(path);
        //create obj for hssfworkbook
        Workbook workbook = new HSSFWorkbook(inputStream);
        //sheetName = workbook.getSheetName(0);
        //get access to sheet
        Sheet sheet = workbook.getSheet(sheetName);

        Iterator<Row> rows = sheet.iterator();
        Row firstrow = rows.next();
        Iterator<Cell> cells = firstrow.cellIterator();

        int k=0;
        while (cells.hasNext()) {
            cells.next();
            //scan from the next row , iterate the cells first store the value of cells in object[][] array.
            k++;
        }
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        Object[][] res = new Object[rowCount][k]; //obj[rowcount][colcount]

        int i =0,j=0;
        while (rows.hasNext()) {
            Row row = rows.next();
            cells = row.cellIterator();
            while (cells.hasNext()){
                Cell value = cells.next();
                if(value.getCellType()== CellType.STRING) {
                    res[i][j] = value.getStringCellValue();
                }
                else {
                    res[i][j] = NumberToTextConverter.toText(value.getNumericCellValue());

                }
                j++;
            }
            i++;
            j=0;
        }

        for(int x=0 ; x< rowCount; x++) {
            for( int y=0; y<k ; y++) {
                System.out.println(res[x][y]);
            }
        }
       return res;

    }

    public static void  takeScreenShot(WebDriver driver) throws IOException {
        Date date  = new Date();
        screenshotName = date.toString().replace(":","_").replace(" ","_") + ".jpg";
        File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(System.getProperty("user.dir") + "\\test-output\\html\\" + screenshotName));
    }
}
