package com.proj.testcases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BankManagerLoginTest extends TestBase{

    @Test
    public void loginAsBankManager() {
        logger.debug("Logging as bank manager");
        driver.findElement(By.cssSelector(OR.getProperty("managerLogin"))).click();
        Assert.assertTrue(isElementPresent(By.cssSelector(OR.getProperty("addCustomer"))));
        driver.findElement(By.cssSelector(OR.getProperty("addCustomer"))).click();
    }
}
