package com.proj.testcases;

import com.aventstack.extentreports.model.Report;
import com.proj.utlities.ExcelUtil;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class AddCustomerTest extends TestBase{

    @Test(dataProvider = "getData")
    public void addCustomer(String firstName, String lastName, String postCode) throws IOException {
        System.setProperty("org.uncommons.reportng.escape-output","false"); // to display screenshot in report ng
       System.out.println(firstName +" "+ lastName +" "+ postCode);
       String alertMessage;
       String path = System.getProperty("user.dir\\") + firstName +".png";
       driver.findElement(By.cssSelector(OR.getProperty("firstName"))).sendKeys(firstName);
       driver.findElement(By.cssSelector(OR.getProperty("lastName"))).sendKeys(lastName);
       driver.findElement(By.cssSelector(OR.getProperty("postCode"))).sendKeys(postCode);
       //Assert.fail("Force Fail");
       driver.findElement(By.cssSelector(OR.getProperty("submit"))).click();
       try {
         Alert alert = driver.switchTo().alert();
         alertMessage = alert.getText();
         System.out.println(alertMessage);
         alert.accept();
         driver.switchTo().defaultContent();
         //  Assert.fail("Failing");
       }
       catch (NoAlertPresentException exception) {
           exception.printStackTrace();
       }

    }

    @DataProvider
    public Object[][] getData() throws IOException {
        return ExcelUtil.getExcel("C:\\Users\\1646238\\IdeaProjects\\AutomationLiveProjects\\src\\main\\resources\\excel\\testdata.xls",
                "AddCustomer");
    }



}
