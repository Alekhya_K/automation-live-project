package com.proj.testcases;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver;
    public static Properties OR = new Properties();
    public static Properties config = new Properties();
    public static FileInputStream inputStream;
    public static Logger logger = LogManager.getLogger("devlogs");

    @BeforeSuite
    public static void setUp() {
        logger.debug("Inside setup method");

        if(driver == null) {
            try {
                inputStream = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\resources\\properties\\OR.properties");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                OR.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream = new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\resources\\properties\\Config.properties");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                config.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(config.getProperty("browser").equals("chrome")) {
                System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
                driver = new ChromeDriver();
            } //add else if , if you want to try other browsers
            driver.get(config.getProperty("testsiteurl"));
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(Integer.parseInt(config.getProperty("implict.wait")), TimeUnit.SECONDS);

        }

    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        }
        catch (NoSuchElementException exception)
        {
            exception.printStackTrace();
            return false;
        }
    }


    @AfterSuite
    public void tearDown() {
        logger.debug("Closing the windows");
    driver.quit();
    }
}
