package com.proj.listeners;

import com.proj.testcases.TestBase;
import com.proj.utlities.ExcelUtil;
import lombok.SneakyThrows;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.io.IOException;

public class CustomListeners extends TestBase implements ITestListener{

    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @SneakyThrows
    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.setProperty("org.uncommons.reportng.escape-output","false"); // to display screenshot in report ng
        Reporter.log("Capturing Screenshot");
        try {
            ExcelUtil.takeScreenShot(driver);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //doubt - Screen shot is not opening in report log(ReportNG)
        Reporter.log("<a target='_blank' href="+ExcelUtil.screenshotName+">Screenshot</a>"); //by default the path will be test-output/html
        //add failure screen shot here

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
