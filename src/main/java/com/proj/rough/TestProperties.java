package com.proj.rough;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

public class TestProperties {
    public static void main(String args[]) throws IOException {
        /*FileInputStream inputStream = new FileInputStream("C:\\Users\\1646238\\IdeaProjects\\AutomationLiveProjects\\src\\main\\resources\\properties\\OR.properties");
        Properties config = new Properties();
        config.load(inputStream);
        System.out.println(System.getProperty("user.dir"));*/
        getExcel("s","s");


    }

    public static void getExcel(String path, String sheetName) throws IOException {
        //identify the firstname column

        path = "C:\\Users\\1646238\\IdeaProjects\\AutomationLiveProjects\\src\\main\\resources\\excel\\testdata.xls";
        FileInputStream inputStream = new FileInputStream(path);
        //create obj for hssfworkbook
        Workbook workbook = new HSSFWorkbook(inputStream);
        sheetName = workbook.getSheetName(0);
        //get access to sheet
        Sheet sheet = workbook.getSheet(sheetName);

        Iterator<Row> rows = sheet.iterator();
        Row firstrow = rows.next();
        Iterator<Cell> cells = firstrow.cellIterator();

        int k=0;
        while (cells.hasNext()) {
          cells.next();
               //scan from the next row , iterate the cells first store the value of cells in object[][] array.
          k++;
        }
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        Object[][] res = new Object[rowCount][k]; //obj[rowcount][colcount]

        int i =0,j=0;
        while (rows.hasNext()) {
            Row row = rows.next();
            cells = row.cellIterator();
            while (cells.hasNext()){
                Cell value = cells.next();
                res[i][j] = value.getStringCellValue();
                j++;
            }
            i++;
            j=0;
        }

        for(int x=0 ; x< rowCount; x++) {
            for( int y=0; y<k ; y++) {
                System.out.println(res[x][y]);
            }
        }


    }
}
